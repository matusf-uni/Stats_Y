# Probability

## Basic definitions
* **Experiment** is any process that requires some action be performed and has
  outcome that can be recorded
* **Outcome** - single result from an experiment
* **Sample space** - a set of all possible outcomes of an experiment.
  (denoted by $S$)
* **Event** - any collection of outcomes from a sample space. Subset of $S$
  I.e. $X \subset S$


### Set theory
* **Universal set** - set of all subsets
* **Distributive laws**
  * $(A \cup B) \cap C = (A \cap C) \cup (A \cap B)$  
  * $A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$

### Probability rules
* $P(S) = 1$
* $P(\emptyset) = 0$
* $P(\bar{A}) = 1 - P(A)$
* $P(A \cup B) = P(A) + P(B) - P(A \cap B)$

### Conditional Probability
* **DEF:** for any events $A$ and $B$, the conditional probability that event $A$ occurs given that event $B$ happened is
* $P(A|B) = \frac{P(A \cap B)}{P(B)}$

### Independence
* **DEF:** events $A$ and $B$ are independent if
* $P(A \cap B) = P(A)P(B)$
* Therefore $P(A \cap B) =$
* $P(A|B) P(B) = P(B|A)  P(A)$

### Bayes Theorem
* $P(B|A) = \frac{P(A|B)P(B)}{P(A)}$
* **Partitions of sample space**
  * $H_{i} \cap H_{j} = \emptyset$
  * $\cup_{i}H_{i} = S$
  * $P(H_{i}) \gt 0$
* **Total probability formula**
  * $P(A) = P(A|H_{1})P(H_{1}) + P(A|H_{2})P(H_{2}) + P(A|H_{3})P(H_{3}) + ... + P(A|H_{j})P(H_{j})$


---
## Discrete Random Variables
### Probability Mass Function (PMF)
* **PMF** $p(x)$ specifies probability that a random variable $X = x(s_{i})$
  for all outcomes $x(s_{i})$ in sample space
* defined on discrete random variables
* Properties
  * $p(x) \gt 0$

  * $\sum_{x \in S} p(x) = 1$
* $p(x) = P(X = x(s_{i})) = P(X = x)$

### Cumulative Distribution Function
* **CDF** represents the probability that a realization from a random variable
  is less than or equal to any real number $r$

* $F(r) = P(X \leq r) = \sum_{y \in S | y \leq r} p(y)$
* Properties
  * $F(-\infty) = 0$
  * $F(\infty) = 1$
  * if $a \leq b$ then $F(A) \leq F(B)$

### Expectation
* Expectation of **discrete** random variable $X$ and PMF is

* $E(X) = \sum_{x \in S}  x p(x)$
* **Properties**
  * $E(X + Y) = E(X) + E(Y)$
  * $E(a) = a$
  * $E(aX) = aE(X)$
  * $E(aX + B) = aE(X) + b$
  * if $X$ and $Y$ are independent
    * $E(XY) = E(X)E(Y)$

### Variance
* measure how things are spread out
* $Var(X) = \sigma^{2}_{x} = E[(X-E(X))^2]$
* $Var(X) = E(x^2) - E(X)^2$

* **Properties**
  * $Var(a) = 0$
  * $Var(X) \geq 0$
  * $Var(X) = 0$ only if $X$ is constant
  * $Var(bX + a) = b^2 Var(X)$
  * if $X$ and $Y$ are independent
    * $Var(aX+bY) = a^2Var(x) + b^2Var(Y)$
    * $Var(X-Y) = Var(x) + Var(Y)$    

## Discrete distributions
### Bernoulli
  * models situations in which there are only two possible outcome
  * $X$ ~ $Bern(\theta)$
  * Situations
    * tossing a coin
    * result of an exam question
    * player scores or not

### Binomial
* Random **binomial** variable $X$ denoted by $X$ ~ $Bi(n, \theta)$ is simply $\sum_{i=2}^{n}Z_{i}$, where $Z_{i}$ is Bernoulli RV
* **PMF**
  * $^{n}C_{x}\theta^{x}(1-\theta)^{n-x}$
* Situations
  * tossing a coin 100 times
  * number of students that pass course

### Geometric
* the number of trials required to obtain the first success
* Situations
  * tossing coin to first head / tail

### Negative binomial
* the number of trials required to get the $k$th
* $X$ ~ $NB(k, \theta)$

### Poisson
* model the number of events that occur in a fixed amount of time or space. The parameter $\mu$ represents both the mean and variance
* **Sum of independent Poisson RV has a Poisson distribution**
* if $X$ ~ $Po(\mu)$ and $Y$ ~ $Po(\lambda)$ then
* $X+Y$ ~ $Po(\mu + \lambda)$
* Situations
  * number of deaths in year
  * number of car accidents

### Hyper-geometric
* $M$ of type 1, $N-M$ of type 2
* Sampling $n$ elements without replacement from a larger
  population containing only two types of items.

## Continuous Random Variables
* **DEF:** A continuous random variable $X(s)$ is a random
 variable whose sample space $S$ has an uncountable number of
 outcomes

### Probability Density Function (PDF)
* $f(x)$
* defined so that probability of being in the interval $[r, s]$ is given by

* $P(r \leq X \leq s) = \int_{r}^{s}f(x)dx$
* $P(X = r) = 0$
* **Properties**
  * $f(x) > 0$
  * $\int_{-\infty}^{\infty} f(x)dx = 1$ => $P(S) = 1$

### Cumulative Distribution Function (CDF)
* $F(x)$
* $F(r)$ is defined as
* $F(r) = P(X \leq r) = \int_{-\infty}^{r}f(x)dx$
* **Properties**
  * $F(-\infty) = 0$
  * $F(\infty) = 1$
  * if $r \leq s$ then $F(r) \leq F(s)$
* $F$ is the integral of $f$
* $f$ is the derivative of $F$
* $P(X \leq r) = P(X = r)$

### Expectation & Variance
* $E(X) = \int_{-\infty}^{\infty} xp(x) dx$
* $E(t(X)) = \int_{-\infty}^{\infty} t(x)p(x) dx$
* $Var(X)  = E(X^2) - E(X)^2$
* **Properties** are the same as with discrete random variables

## Continuous Distributions
### Gaussian or Normal distribution
* $X$ ~ $N(\mu, \sigma^2)$ where $\mu$ is mean and $\sigma^2
  is variance$
* Situations
  * population characteristics (height, weight)
  * scientific experiments measurements made from experiments
    that are subject to error.
  * statistical methods - it is the basis for a number of statistical methods.
* **Properties**
  * if $X$ ~ $N(\mu, \sigma^2)$ and $a, b$ are constants, then
  * $(aX + b)$ ~ $N(a\mu + b, a^2\sigma^2)$
  * if $X_{1}, X_{2}$ are independent normally distributed
    * $X_{1} + X_{2}$ ~ $N(\mu_{1} + \mu_{2}, \sigma_{1}^2 + \sigma_{2}^2)$
    * $X_{1} - X_{2}$ ~ $N(\mu_{1} - \mu_{2}, \sigma_{1}^2 + \sigma_{2}^2)$

#### Standard Norma Distribution
* has mean 0 and variance 1
* denoted by $Z$ ~ $N(1,0)$

#### Standardization
* $Z = \frac{X- \mu}{\sigma}$ ~ $N(0, 1)$

## Central Limit Theorem
* Let's have a sequence of independent, identically distributed random variables, each with finite mean and finite variance. Then for sufficiently large $n$ we have that
* $\sum_{i = 1}^{n} X_{i}$ ~ $N(n\mu, n\sigma^2)$

### Normal approximation to the Binomial
* Consider $X$ ~ $Bi(1000, 0.4)$ and suppose you wish to calculate $P(X > 661)$. The shortest way is to calculate
* $P(X = 662) + P(X = 663) + ... + P(X = 1000)$
* Variance and expectation for binomial is
* $E(X) = \theta, Var(X) = \theta(1-\theta)$
* From **CLT** $Bi(n, \theta)$ ~ $N(n\theta, n\theta(1- \theta))$

#### Continuity Correction
* for continuous random variables $P(X \leq r) = P(X < r)$
* this is not true for discrete random variables
* therefore we need to do following correction
* $P(X > r)$ is replaced with $P(X > r + 0.5)$.
* $P(X \geq r)$ is replaced with $P(X \geq r − 0.5)$.
* $P(X < r)$ is replaced with $P(X < r − 0.5)$.
* $P(X \leq r)$ is replaced with $P(X \leq r + 0.5)$.

### Normal approximation to Poisson
* Let $X_{1}, X_{2}, X_{3} ... X_{n}$ be independent and identical $Po(\mu)$ random Variables. We know that
  * $E(X_{i}) = \mu$
  * $Var(X_{i}) = \mu$
  * sum of these variables $X = \sum_{i=1}^{n} X_{i}$ ~ $Po(n\mu)$
* From **CLT** $\sum_{i = 1}^{n} X_{i}$ ~ $N(n\mu, n\sigma^2)$
  * therefore $Po(n\mu)$ ~ $N(n\mu, n\mu)$
  * we have to do continuity correction

### Uniform Distribution
* uniform random variable with sample space is denoted by
* $X$ ~ $U[a, b]$
* each outcome in the sample space has the same probability
* $f(x) = \frac{1}{b-a}$ if $a \leq x \leq b$ else $0$
* $E(X) = \frac{a+b}{2}$
* $Var(X) = \frac{(b-a)^2}{12}$
* Situations
  * directions (wind)
  * equally likely outcomes
